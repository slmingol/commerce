package matlik.commerce

import matlik.measures._
import scala.math.BigDecimal
import scala.math.BigDecimal.RoundingMode._

trait QUnit extends UOM

case class Quantity(val value: BigDecimal, val unit: QUnit) extends Quantafiable[QUnit] {
  override type selfType = Quantity
  override def uom = unit
  override def qty = value

  override def +(that: Quantity)(implicit converter: Converter[Quantity, QUnit]): Quantity = {
    val converted = if (this.uom == that.uom) that else converter.convert(that, uom)
    this.copy(value = value + converted.qty)
  }

  override def +(that: BigDecimal): Quantity = this.copy(value = value + that)

  override def -(that: Quantity)(implicit converter: Converter[Quantity, QUnit]): Quantity = {
    val converted = if (this.uom == that.uom) that else converter.convert(that, uom)
    this.copy(value = value - converted.qty)
  }

  override def -(that: BigDecimal): Quantity = this.copy(value = value - that)

  override def *(that: BigDecimal): Quantity = this.copy(value = value * that)

  override def /(that: BigDecimal): Quantity = this.copy(value = value / that)

  override def round(
    roundingMode: RoundingMode = uom.defaultRoundingMode,
    minimumValue: BigDecimal = uom.minimumValue,
    precision: Int = uom.significantDecimalDigits): Quantity = ???
}

object Quantity {
  implicit object SimpleConverter extends Converter[Quantity, QUnit] {
    override def convert(qty: Quantity, uom: QUnit): Quantity = {
      if (qty.uom == uom) return qty
      val converted = findConversionRate(qty.uom, uom).map(rate => Quantity(qty.qty * rate, uom))
      converted getOrElse sys.error("No conversion found from " + qty.uom + " to " + uom)
    }

    override def findConversionRate(from: QUnit, to: QUnit): Option[BigDecimal] = {
      val lookup = Map { ("Each", "Each") -> BigDecimal("1") }
      lookup.get((from.shortLabel, to.shortLabel))
    }
  }

  object EACH extends QUnit {
    val longLabel = "Each"
    val shortLabel = "Each"
    val symbol = "Each"
    val significantDecimalDigits = 20
    val minimumValue = BigDecimal("1")
    override val defaultRoundingMode = UNNECESSARY
  }
}