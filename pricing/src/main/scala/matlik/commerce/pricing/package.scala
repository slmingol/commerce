package matlik.commerce

package object pricing {
  type EntryPriceSeq = Seq[EntryPrice]
  type Results = Seq[Seq[CalculationStepResult]]
  type ApplicationResultGroup = Seq[ApplicationResult]
}