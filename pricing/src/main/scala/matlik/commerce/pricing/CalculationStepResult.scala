package matlik.commerce.pricing

import matlik.measures.Money
import matlik.commerce.Quantity
import matlik.common.ValidityDates
import matlik.common.Message
import matlik.common.Messages
import matlik.commerce.pk.EntryPK
import org.joda.time.DateTime
import matlik.commerce.pk.CalculationStepPK

/**
 * A CalculationResult
 */
trait CalculationResult extends Messages

/** Result visible outside the price step */
trait PriceResult extends CalculationResult

/** Result visible within the price step */
trait CalculationStepResult extends CalculationResult {
  def step: Option[CalculationStepPK]
  def depth: Option[Int]
}

/** A result without any direct impact on the pricing document */
trait NoOp extends CalculationResult

///** A result that has direct impact to the pricing document */
//trait Applied {
//  def consumesQty: Boolean
//}

//trait Adjusted extends CalculationStepResult

trait RestrictionResult extends CalculationStepResult
trait ApplicationResult extends CalculationStepResult {
  def applies: Boolean
}

trait EntryLevel {
  def entry: EntryPK
}

trait EntryPriceLevel extends EntryLevel {
  def entryPrice: EntryPriceID
}

trait EntryPriceMutation extends EntryLevel {
  def fromEntryPrice: Seq[EntryPriceID]
  def toEntryPrice: EntryPriceID
  def unitPrice: Option[Money]
}

trait QuantafiableResult {
  def quantity: Quantity
}

//trait Promotion {
//  def promotionId: Long // TODO: PromotionId
//}

trait AggregatePriceStepResults {
  def childResults: Seq[CalculationStepResult]
}

/** Requirements either pass or fail at the course grained order and step level */
trait StepRestriction extends CalculationStepResult
case class StepAllowed(step: Option[CalculationStepPK], depth: Option[Int], messages: Seq[Message] = Seq.empty) extends StepRestriction
case class StepDenied(step: Option[CalculationStepPK], depth: Option[Int], messages: Seq[Message] = Seq.empty) extends StepRestriction

/** ItemRestrictions deny or allow individual item (rows in the document) from subsequent processing */
trait EntryPriceRestriction extends CalculationStepResult with EntryPriceLevel
case class EntryPriceAllowed(step: Option[CalculationStepPK], depth: Option[Int], entry: EntryPK, entryPrice: EntryPriceID, messages: Seq[Message] = Seq.empty) extends EntryPriceRestriction {
  def this(step: Option[CalculationStepPK], depth: Option[Int], entryPrice: EntryPrice) = this(step, depth, entryPrice.entry, entryPrice.id.get)
}
case class EntryPriceDenied(step: Option[CalculationStepPK], depth: Option[Int], entry: EntryPK, entryPrice: EntryPriceID, messages: Seq[Message] = Seq.empty) extends EntryPriceRestriction {
  def this(step: Option[CalculationStepPK], depth: Option[Int], entryPrice: EntryPrice) = this(step, depth, entryPrice.entry, entryPrice.id.get)
}

/** Applications describe how a promotion applies to entries */
case class EntryApplication(
  step: Option[CalculationStepPK],
  depth: Option[Int],
  entry: EntryPK,
  fromEntryPrice: Seq[EntryPriceID],
  toEntryPrice: EntryPriceID,
  quantity: Quantity,
  unitPrice: Option[Money],
  startDate: Option[DateTime],
  endDate: Option[DateTime],
  applies: Boolean,
  messages: Seq[Message] = Seq.empty) extends ApplicationResult with EntryPriceMutation with QuantafiableResult with ValidityDates {
  def this(context: CalculationContext, entryPrice: EntryPrice, quantity: Option[Quantity] = None, applies: Boolean = true, messages: Seq[Message] = Seq.empty) = this(
    step = context.calculationStep,
    depth = context.calculationDepth,
    entry = entryPrice.entry,
    fromEntryPrice = Seq(entryPrice.id.get),
    toEntryPrice = new EntryPriceID(),
    quantity = quantity.getOrElse(entryPrice.quantity),
    startDate = entryPrice.startDate,
    endDate = entryPrice.endDate,
    applies = applies,
    unitPrice = entryPrice.unitPrice,
    messages = messages)
}

case class CompoundApplication(
  step: Option[CalculationStepPK],
  depth: Option[Int],
  childResults: Seq[CalculationStepResult],
  applies: Boolean,
  messages: Seq[Message] = Seq.empty) extends ApplicationResult with AggregatePriceStepResults