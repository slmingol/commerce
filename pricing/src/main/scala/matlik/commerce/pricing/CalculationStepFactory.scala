package matlik.commerce.pricing

trait CalculationStepFactory {
  def load(context: CalculationContext): CalculationStep
}