package matlik.commerce.pricing

import org.joda.time.DateTime
import org.junit.runner.RunWith
import org.scalatest.WordSpec
import org.scalatest.Matchers
import org.scalacheck._
import org.scalacheck.Arbitrary._
import org.scalacheck.Gen._
import org.scalacheck.Prop._
import matlik.commerce.Quantity
import matlik.commerce.Quantity._
import matlik.measures.Money
import matlik.measures.SimpleImplicits
import matlik.measures.SimpleImplicits._

@RunWith(classOf[org.scalatest.junit.JUnitRunner])
class CalculationStepSequenceSpec extends WordSpec with Matchers {

  "CalculationStepSequence object" should {
    "suppport StepRestriction" in {
      pending
    }
    
    "call all steps in the sequence without stacking" in {
      pending
    }
  }

}