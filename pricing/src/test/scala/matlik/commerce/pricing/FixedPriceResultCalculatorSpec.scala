package matlik.commerce.pricing
import org.joda.time.DateTime
import org.junit.runner.RunWith
import org.scalatest.WordSpec
import org.scalatest.Matchers
import matlik.measures._
import matlik.measures.SimpleImplicits._
import matlik.commerce.Quantity
import matlik.commerce.Quantity._
import matlik.commerce.pk._
import matlik.commerce.pricing.CalculationStep._
import matlik.common.Message

@RunWith(classOf[org.scalatest.junit.JUnitRunner])
class FixedPriceResultCalculatorSpec extends WordSpec with Matchers {

  import matlik.measures.SimpleImplicits._

  val context = SimpleCalculationContext(stepView = Seq.empty[EntryPrice])

  val ep1 = SimpleEntryPrice(id = Option(new EntryPriceID()), entry = EntryID(1), quantity = Quantity(1, EACH), unitPrice = Option(Money(10, USD)))
  val ep2 = SimpleEntryPrice(id = Option(new EntryPriceID()), entry = EntryID(2), quantity = Quantity(2, EACH), unitPrice = Option(Money(20, USD)))
  val ep3 = SimpleEntryPrice(id = Option(new EntryPriceID()), entry = EntryID(3), quantity = Quantity(3, EACH), unitPrice = Option(Money(30, USD)))
  val ep3Z = SimpleEntryPrice(id = Option(new EntryPriceID()), entry = EntryID(4), quantity = Quantity(3, EACH), unitPrice = Option(Money(30, USD)))

  val ea1 = new EntryApplication(context, ep1)
  val ea2 = new EntryApplication(context, ep2)
  val ea3 = new EntryApplication(context, ep3)
  val ea3Z = new EntryApplication(context, ep3Z)

  val ar = new ApplicationResult() {
    val step = Option(CalculationStepID(50))
    val depth = Option(1)
    val applies = true
    def messages = Nil
  }

  "Standard ShareDistribution implementations" should {
    "support quantity based distributions (QuantityBasedShareDistribution)" in {
      val dist = QuantityBasedShareDistribution
      assertResult(Seq())(dist.getSharesOfDistribution(context, Seq()))
      assertResult(Seq(1.0, 2.0, 3.0))(dist.getSharesOfDistribution(context, Seq(ea1, ea2, ea3)))

      val ar = new ApplicationResult() {
        val step = Option(CalculationStepID(50))
        val depth = Option(1)
        val applies = true
        def messages = Nil
      }

      intercept[ClassCastException] {
        dist.getSharesOfDistribution(context, Seq(ar))
      }
    }

    "support price based distributions (PriceBasedShareDistribution)" in {
      val dist = PriceBasedShareDistribution
      assertResult(Seq())(dist.getSharesOfDistribution(context, Seq()))
      assertResult(Seq(10.0, 40.0, 90.0))(dist.getSharesOfDistribution(context, Seq(ea1, ea2, ea3)))

      intercept[ClassCastException] {
        dist.getSharesOfDistribution(context, Seq(ar))
      }
    }
  }

  "AbstractDistributedFixedPriceResultCalculator" should {
    val dummy = new AbstractDistributedFixedPriceResultCalculator() {
      override def getSharesOfDistribution(context: CalculationContext, group: ApplicationResultGroup): Seq[Double] = {
        for (e <- group) yield {
          e match {
            case y if y == ea3Z => 0.0
            case x: EntryApplication => x.quantity.qty.toDouble
          }
        }
      }
      override def calculateUnitPrice(context: CalculationContext, result: ApplicationResult, priceRow: FixedPriceRow, unitPrice: Money)(implicit converter: Converter[Money, Currency]): Money = {
        unitPrice
      }
    }

    "calculate a valid total price for entry based on share" in {
      assertResult(Money(5, USD))(dummy.determineTotalShareForEntry(Quantity(1, EACH), Money(5, USD), 1.0))
      assertResult(Money(5, USD))(dummy.determineTotalShareForEntry(Quantity(1, EACH), Money(1, USD), 5.0))
      assertResult(Money(0.99, USD))(dummy.determineTotalShareForEntry(Quantity(3, EACH), Money(1, USD), 1.0))
      assertResult(Money(2.01, USD))(dummy.determineTotalShareForEntry(Quantity(3, EACH), Money(2, USD), 1.0))
      assertResult(Money(0, USD))(dummy.determineTotalShareForEntry(Quantity(1, EACH), Money(5, USD), 0))
      assertResult(Money(0, USD))(dummy.determineTotalShareForEntry(Quantity(1, EACH), Money(0, USD), 1.0))
    }

    "audit for preconditions" in {
      assertResult(Seq())(dummy.calculateResults(context, Seq(), Seq()))

      intercept[AssertionError] {
        dummy.calculateResults(context, Seq(), Seq(SimplePercentPriceRow(100)))
      }

      intercept[AssertionError] {
        dummy.calculateResults(context, Seq(Seq(ea1)), Seq())
      }

      intercept[ClassCastException] {
        dummy.calculateResults(context, Seq(Seq(ea1)), Seq(SimplePercentPriceRow(100)))
      }

      intercept[Exception] {
        dummy.calculateResults(context, Seq(Seq(ar)), Seq(SimpleFixedPriceRow(Money(100, USD))))
      }

    }

    "distribute accurately at the unit level" in {
      val group = Seq(ea1, ea2, ea3)
      val groupZ = Seq(ea1, ea2, ea3Z)

      def getUnitPrice(a: CalculationResult): Money = {
        a.asInstanceOf[EntryApplication].unitPrice.get
      }

      def toEA(a: CalculationResult) = a.asInstanceOf[EntryApplication]

      // Simple and even distribution
      val result1 = dummy.calculateResults(context, Seq(group), Seq(SimpleFixedPriceRow(Money(60, USD))))
      assertResult(3)(result1.size)
      assertResult(Money(10, USD))(getUnitPrice(result1(0)))
      assertResult(Money(10, USD))(getUnitPrice(result1(1)))
      assertResult(Money(10, USD))(getUnitPrice(result1(2)))

      // Simple and even distribution with last entry with zero share
      val result1Z = dummy.calculateResults(context, Seq(groupZ), Seq(SimpleFixedPriceRow(Money(60, USD))))
      assertResult(3)(result1.size)
      assertResult(Money(20, USD))(getUnitPrice(result1Z(0)))
      assertResult(Money(20, USD))(getUnitPrice(result1Z(1)))
      assertResult(Money(0, USD))(getUnitPrice(result1Z(2)))

      // Distribution with remainder
      val result2 = dummy.calculateResults(context, Seq(group), Seq(SimpleFixedPriceRow(Money(100, USD))))
      assertResult(4)(result2.size)
      assertResult(ea1.fromEntryPrice)(toEA(result2(0)).fromEntryPrice)
      assertResult(ea1.toEntryPrice)(toEA(result2(0)).toEntryPrice)
      assertResult(ea1.quantity)(toEA(result2(0)).quantity)
      assertResult(Money(16.67, USD))(getUnitPrice(result2(0)))

      assertResult(ea2.fromEntryPrice)(toEA(result2(1)).fromEntryPrice)
      assertResult(ea2.toEntryPrice)(toEA(result2(1)).toEntryPrice)
      assertResult(ea2.quantity)(toEA(result2(1)).quantity)
      assertResult(Money(16.67, USD))(getUnitPrice(result2(1)))

      assertResult(ea3.fromEntryPrice)(toEA(result2(2)).fromEntryPrice)
      assertResult(ea3.toEntryPrice)(toEA(result2(2)).toEntryPrice)
      assertResult(Quantity(2, EACH))(toEA(result2(2)).quantity)
      assertResult(Money(16.67, USD))(getUnitPrice(result2(2)))

      assertResult(ea3.fromEntryPrice)(toEA(result2(3)).fromEntryPrice)
      assert(ea3.toEntryPrice != toEA(result2(3)).toEntryPrice)
      assertResult(Quantity(1, EACH))(toEA(result2(3)).quantity)
      assertResult(Money(16.65, USD))(getUnitPrice(result2(3)))

      // Distribution with remainder and last entry with zero share
      val result2Z = dummy.calculateResults(context, Seq(groupZ), Seq(SimpleFixedPriceRow(Money(100, USD))))
      assertResult(4)(result2.size)
      assertResult(ea1.fromEntryPrice)(toEA(result2Z(0)).fromEntryPrice)
      assertResult(ea1.toEntryPrice)(toEA(result2Z(0)).toEntryPrice)
      assertResult(ea1.quantity)(toEA(result2Z(0)).quantity)
      assertResult(Money(33.33, USD))(getUnitPrice(result2Z(0)))

      assertResult(ea2.fromEntryPrice)(toEA(result2Z(1)).fromEntryPrice)
      assertResult(ea2.toEntryPrice)(toEA(result2Z(1)).toEntryPrice)
      assertResult(Quantity(1, EACH))(toEA(result2Z(1)).quantity)
      assertResult(Money(33.33, USD))(getUnitPrice(result2Z(1)))

      assertResult(ea2.fromEntryPrice)(toEA(result2Z(2)).fromEntryPrice)
      assert(ea2.toEntryPrice != toEA(result2Z(2)).toEntryPrice)
      assertResult(Quantity(1, EACH))(toEA(result2Z(2)).quantity)
      assertResult(Money(33.34, USD))(getUnitPrice(result2Z(2)))

      assert(ea3Z.fromEntryPrice == toEA(result2Z(3)).fromEntryPrice)
      assert(ea3Z.toEntryPrice == toEA(result2Z(3)).toEntryPrice)
      assert(ea3Z.quantity == toEA(result2Z(3)).quantity)
      assert(Money(0, USD) == getUnitPrice(result2Z(3)))

    }
  }

  "Standard FixedUnitPriceCalculator implementations" should {
    "support adjusting to a specified non-negative value (FixedUnitPrice)" in {
      intercept[AssertionError] {
        FixedUnitPrice.calculateUnitPrice(context, ea1, SimpleFixedPriceRow(Money(100, USD)), Money(-1, USD))
      }

      assert(Money(0, USD) == FixedUnitPrice.calculateUnitPrice(context, ea1, SimpleFixedPriceRow(Money(100, USD)), Money(0, USD)))
      assert(Money(1, USD) == FixedUnitPrice.calculateUnitPrice(context, ea1, SimpleFixedPriceRow(Money(100, USD)), Money(1, USD)))
    }

    "support adjusting prices up when positive, bottoming at zero (FixedUnitPriceUplift)" in {
      assert(Money(11, USD) == FixedUnitPriceUplift.calculateUnitPrice(context, ea1, SimpleFixedPriceRow(Money(100, USD)), Money(1, USD)))
      assert(Money(9, USD) == FixedUnitPriceUplift.calculateUnitPrice(context, ea1, SimpleFixedPriceRow(Money(100, USD)), Money(-1, USD)))
      assert(Money(0, USD) == FixedUnitPriceUplift.calculateUnitPrice(context, ea1, SimpleFixedPriceRow(Money(100, USD)), Money(-10, USD)))
      assert(Money(0, USD) == FixedUnitPriceUplift.calculateUnitPrice(context, ea1, SimpleFixedPriceRow(Money(100, USD)), Money(-11, USD)))
    }

    "support adjusting prices down when positive, bottoming out at zero (FixedUnitPriceDiscount)" in {
      assert(Money(9, USD) == FixedUnitPriceDiscount.calculateUnitPrice(context, ea1, SimpleFixedPriceRow(Money(100, USD)), Money(1, USD)))
      assert(Money(11, USD) == FixedUnitPriceDiscount.calculateUnitPrice(context, ea1, SimpleFixedPriceRow(Money(100, USD)), Money(-1, USD)))
      assert(Money(0, USD) == FixedUnitPriceDiscount.calculateUnitPrice(context, ea1, SimpleFixedPriceRow(Money(100, USD)), Money(10, USD)))
      assert(Money(0, USD) == FixedUnitPriceDiscount.calculateUnitPrice(context, ea1, SimpleFixedPriceRow(Money(100, USD)), Money(11, USD)))
    }
  }

}